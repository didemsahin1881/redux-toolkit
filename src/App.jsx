import { useState } from 'react' 
import './App.css'
import {useSelector, useDispatch} from 'react-redux';
import { decrement, increment } from './redux/counterSlice';
import UserList from './UserList';

function App() {
  const dispatch = useDispatch();
  const {value} = useSelector((store)=> store.counter);
   
console.log(value);
  return (
    <>
      <div> 
      </div>
      <h1>Vite + React</h1> 
      <h2>{value}</h2> 
      <button onClick={()=> dispatch(increment())}>Artır</button>      
      <button onClick={()=> dispatch(decrement())}>Azalt</button>
      <UserList>
        
      </UserList>
    </>
  )
}

export default App