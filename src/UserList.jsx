import React, { Fragment, useEffect } from 'react'
import {useDispatch, useSelector} from 'react-redux';
import { getAllUsers } from './redux/userSlice';
import User from './User';

const UserList = () => {
  const dispatch = useDispatch();
const {users} = useSelector(store => store.user);
console.log(users);

  useEffect(()=>{
    dispatch(getAllUsers());
  },
  []);
  return (
    <Fragment>

      <h2>UserList</h2>
      {
        users && users.map((user) => (
          <User key={user.id} user={user}></User>
        ))
      }
    </Fragment>
  )
}

export default UserList