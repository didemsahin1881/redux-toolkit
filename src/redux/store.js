import { configureStore } from '@reduxjs/toolkit'
import  counterReducer from '../redux/counterSlice';
import userSlice from './userSlice';
//productSlice

export const store = configureStore({
  reducer: {
    counter: counterReducer,
   // product:productSlice,
    user:userSlice,
  },
})